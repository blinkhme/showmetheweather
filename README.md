Show me the Weather
Author: Kean Hui Chuah
======================
An angular app to display the weather forecast for 
a week starting from the current day. Inspired by the
chrome addon Momentum. Features include

- Display current time
- Personalised greeting depending on time
- Random background on load
- Display weather in Fahrenheit or Celcius
- Autocomplete on location search input field
- Carousel style weather forecast for the week

How To Run
```
npm install
bower install
grunt serve
```
