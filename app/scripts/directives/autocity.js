'use strict';

/**
 * @ngdoc directive
 * @name showMeTheWeatherApp.directive:autoCity
 * @description
 * # autoCity
 */
angular.module('showMeTheWeatherApp')
  .directive('autoCity', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element) {

        var options = {
          types: ['(cities)']
        };
        var scopeSup = scope;
        var autocomplete = new google.maps.places.Autocomplete(element[0], options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
          //  use short name to request API
          scopeSup.search.city = place.address_components[0].short_name;

          // after this, change search button's status | enable
          scopeSup.waiting = false;
          scopeSup.$apply();
        });

      }
    };
  });
