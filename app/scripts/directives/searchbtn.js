'use strict';

/**
 * @ngdoc directive
 * @name showMeTheWeatherApp.directive:searchBtn
 * @description
 * # searchBtn
 */
angular.module('showMeTheWeatherApp')
  .directive('searchBtn', function (weatherDetails) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        // waiting status (for API)
        scope.waiting = true;

        // listen on click event
        element.on('click', function () {
          // show loading... as button text
          var $btn = $(element).button('loading');
          // call MainCtrl api
          weatherDetails(scope.search).success(function (data) {
            scope.notFoundError = false;
            scope.weathers = data;
            scope.unitFlag = (scope.search.units === 'metric' ? '°C' : '°F');

            // show weather details view
            scope.weekFlag = true;
          }).error(function (data, status) {
            //show error message
            if (status === 404) {
              scope.notFoundError = true;
            }
            //  ...
          }).finally(function () {
            // all completed
            $btn.button('reset');
            scope.search.city_hidden = null;
            setTimeout(function() {
              scope.waiting = true;
              scope.$apply();
            }, 0);
          });
        });
      }
    };
  });
