'use strict';

/**
 * @ngdoc service
 * @name showMeTheWeatherApp.weatherDetails
 * @description
 * # weatherDetails
 * Factory in the showMeTheWeatherApp.
 */
angular.module('showMeTheWeatherApp')
  .factory('weatherDetails', function ($http) {
    // basic API url
    var apiUrl = 'http://api.openweathermap.org/data/2.5/forecast/daily?cnt=7&callback=JSON_CALLBACK';

    // Public API here
    return function (search) {
      if (search && search.city) {
        apiUrl += '&q=' + search.city + '&units=' + search.units;
        // cross domain request
        return $http.jsonp(apiUrl);
      }
    };
  });
