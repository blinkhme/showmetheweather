'use strict';

/**
 * @ngdoc service
 * @name showMeTheWeatherApp.bgurl
 * @description
 * # get the background image based on the current city's weather
 * Factory in the showMeTheWeatherApp.
 */
angular.module('showMeTheWeatherApp')
  .factory('bgurl', function () {
    var bg = Math.floor((Math.random() * 10) + 1);
    return '/images/bg' + bg + '.jpg';
  });
