'use strict';

/**
 * @ngdoc function
 * @name showMeTheWeatherApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the showMeTheWeatherApp
 */
angular.module('showMeTheWeatherApp')
  .controller('MainCtrl', function ($scope, $location) {
    $scope.notFoundError = false;
    $scope.search = {};
    $scope.search.units = 'metric';
    $scope.timeNow = new Date();
    $scope.username = 'SapientNitro';

    // data results object
    $scope.weathers = {};

    // flag for displaying weekly forecast
    $scope.weekFlag = false;

    // active tab
    $scope.isActive = function(route) {
      return route === $location.path();
    };

    // week list details
    $scope.showWeek = function () {
      return !$scope.notFoundError && $scope.weekFlag;
    };
  });
