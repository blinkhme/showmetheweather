'use strict';

/**
 * @ngdoc function
 * @name showMeTheWeatherApp.controller:BgCtrl
 * @description
 * # Background controller
 * Controller of the showMeTheWeatherApp
 */
function BgCtrl($scope, bgurl) {
    $scope.bg = bgurl;
}