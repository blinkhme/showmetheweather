'use strict';

/**
 * @ngdoc overview
 * @name showMeTheWeatherApp
 * @description
 * # showMeTheWeatherApp
 *
 * Main module of the application.
 */
angular
  .module('showMeTheWeatherApp', [
    'ngRoute',
    '720kb.background'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
